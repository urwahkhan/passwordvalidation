
package Lab8;
/**
 *
 * @author urwah
 */
public class PasswordValidator {
    private static boolean checkLength(String password){
        return password.length()>= 8;    
    }  
    public static boolean checkUpperCaseChars( String password){
        return !password.equals(password.toLowerCase());
    }
    public static boolean checkSpecialChars(String password){
       return false; // @$+!#?^&
    }
    public static boolean checkDigits(String password){
        return password.matches(".*\\d.*");
    }
    public static boolean validatePassword(String password){
       return checkLength(password) && checkSpecialChars(password)
      && checkUpperCaseChars(password) && checkDigits(password);
    
               
    }   
}
