/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author urwah
 */
public class StudentController {
    Student model;
    StudentView view;
    public StudentController(Student model, StudentView view){
        this.model=model;
        this.view=view;
    }
    public void updateView(){
        view.printStudentDetails(model);
    }
}
