/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Lab8;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author urwah
 */
public class PasswordValidatorTest {
    
    public PasswordValidatorTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
   //*************************************************Length Test**********************************/
    @Test
    public void testValidatePassword() {
        System.out.println("validatePassword length regular");
        String password = "abvvvvij";
        boolean expResult = true;
        boolean result = PasswordValidator.validatePassword(password);
        assertEquals("The length of the password doesnot meet the requirements",expResult, result);
       
    }
    @Test
    public void testValidatePasswordLengthException() {
        System.out.println("validatePassword length exception");
        String password = "";
        boolean expResult = false;
        boolean result = PasswordValidator.validatePassword(password);
        assertEquals("The length of the password doesnot meet the requirements",expResult, result);
    
    }
   @Test
    public void testValidatePasswordLengthBoundaryIn() {
        System.out.println("validatePassword length boundary in");
        String password = "abcdefgh";
        boolean expResult = true;
        boolean result = PasswordValidator.validatePassword(password);
        assertEquals("The length of the password doesnot meet the requirements",expResult, result);
       // fail("The test case is a prototype.");
    } 
    @Test
    public void testValidatePasswordLengthBoundaryOut() {
        System.out.println("validatePassword length boundary out");
        String password = "abcdefg";
        boolean expResult = false;
        boolean result = PasswordValidator.validatePassword(password);
        assertEquals("The length of the password doesnot meet the requirements",expResult, result);
    } 
    
    /*************************************************Character Test***********************/
    @Test
    public void testValidatePasswordSpecialCharsRegular() {
        System.out.println("validatePassword special chars regular");
        String password = "abcd@efg";
        boolean expResult = true;
        boolean result = PasswordValidator.validatePassword(password);
        assertEquals("The  password doesnot meet the requirements",expResult, result);
    }    
    @Test
    public void testValidatePasswordSpecialCharsException() {
        System.out.println("validatePassword special exception");
        String password = "";
        boolean expResult = false;
        boolean result = PasswordValidator.validatePassword(password);
        assertEquals("The password doesnot meet the requirements",expResult, result);
      
    }
    @Test
    public void testValidatePasswordSpecialCharsBoundaryIn() {
        System.out.println("validatePassword SpecialChars boundary in");
        String password = "abcd@fgh";
        boolean expResult = true;
        boolean result = PasswordValidator.validatePassword(password);
        assertEquals("The password doesnot meet the requirements",expResult, result);
          } 
    @Test
    public void testValidatePasswordSpecialCharsBoundaryOut() {
        System.out.println("validatePassword SpecialChars boundary out");
        String password = "abcdefg";
        boolean expResult = false;
        boolean result = PasswordValidator.validatePassword(password);
        assertEquals("The password doesnot meet the requirements",expResult, result);
    } 
    /*******************************UpperCase Test************/
    @Test
    public void testValidatePasswordUpperCaseRegular() {
        System.out.println("validatePassword Uppercase regular");
        String password = "aBcd@efg";
        boolean expResult = true;
        boolean result = PasswordValidator.validatePassword(password);
        assertEquals("The  password doesnot meet the requirements",expResult, result);
    }    
    @Test
    public void testValidatePasswordUppercaseException() {
        System.out.println("validatePassword UpperCase exception");
        String password = "";
        boolean expResult = false;
        boolean result = PasswordValidator.validatePassword(password);
        assertEquals("The password doesnot meet the requirements",expResult, result);
       // fail("The test case is a prototype.");
    }
    @Test
    public void testValidatePasswordUpperCaseBoundaryIn() {
        System.out.println("validatePassword UpperCase boundary in");
        String password = "abCd@fgh";
        boolean expResult = true;
        boolean result = PasswordValidator.validatePassword(password);
        assertEquals("The password doesnot meet the requirements",expResult, result);
          } 
    @Test
    public void testValidatePasswordUpperCaseBoundaryOut() {
        System.out.println("validatePassword SpecialChars boundary out");
        String password = "abcDefg";
        boolean expResult = false;
        boolean result = PasswordValidator.validatePassword(password);
        assertEquals("The password doesnot meet the requirements",expResult, result);
    }
    /*******************************Digits Test************/
    @Test
    public void testValidatePasswordDigitsRegular() {
        System.out.println("validatePassword Digits regular");
        String password = "aB2d@efg";
        boolean expResult = true;
        boolean result = PasswordValidator.validatePassword(password);
        assertEquals("The  password doesnot meet the requirements",expResult, result);
    }    
    @Test
    public void testValidatePasswordDigitsException() {
        System.out.println("validatePassword Digit exception");
        String password = "";
        boolean expResult = false;
        boolean result = PasswordValidator.validatePassword(password);
        assertEquals("The password doesnot meet the requirements",expResult, result);
    }
    @Test
    public void testValidatePasswordDigitsBoundaryIn() {
        System.out.println("validatePassword Digits boundary in");
        String password = "abCd@f4h";
        boolean expResult = true;
        boolean result = PasswordValidator.validatePassword(password);
        assertEquals("The password doesnot meet the requirements",expResult, result);
          }
    @Test
    public void testValidatePasswordDigitBoundaryOut() {
        System.out.println("validatePassword SpecialChars boundary out");
        String password = "a7cDefg";
        boolean expResult = false;
        boolean result = PasswordValidator.validatePassword(password);
        assertEquals("The password doesnot meet the requirements",expResult, result);
    }
    
    
}

